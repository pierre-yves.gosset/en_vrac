Gestion avancée d'un calendrier d'événements
=============================================

Problématique
-------------

Une association reçoit de nombreuses invitations à des événements.
Plusieurs personnes sont affectées au tri de ces événements (pertinence).
D'autres personnes peuvent être sollicitées pour répondre aux invitations.
Le problème est qu'il est difficile de suivre : si un événement a bien été trié, s'il a fait l'objet d'une réponse, s'il a été validé/refusé, qui y participe, si les dates ont bien été fixées.

Objectifs
---------------
1. n'utiliser que des outils libres
2. avoir une vision claire (voir graphique) de la liste des invitations reçues, en attente, traitées, refusée, etc
3. avoir un calendrier des événements en ligne
4. pouvoir afficher ce calendrier sur un smartphone, dans une application calendrier standard
5. pouvoir travailler sur ces événements de façon collaborative (ex: que Bob puisse transmettre un événement à Alice ; que Alice puisse noter/partager le fait que c'est Mark qui participera ; etc)
6. pouvoir mettre en place d'éventuels rappels (ex: mail "événement x dans y jour")
7. pouvoir garder trace des événements passés et les exporter


Prérequis
--------------
1. un compte sur Framaboard.org
2. un compte sur Framateam.org
3. un compte sur Framagenda.org
4. un smartphone avec l'application ICSDroid d'installée (passer par F-droid)


Méthodologie
==================

Création du board
-------------------

Framaboard sera l'outil central. 
1. se créer un compte, ainsi qu'un projet "Evenement"
2. éventuellement, créer les comptes d'autres personnes habilitées à modifier la liste des invitations (les passer éventuellement en "Chef de projet")
3. créer les colonnes suivantes ("Préférences > colonnes") : 
  * invitations en attente
  * invitations refusées
  * discussions en cours
  * événements programmés
  * événements passés

Le principe étant que les personnes recevant une invitation la reporte en tant que tâche dans la colonne "invitation en attente".

1. dans le titre, mettre des infos claire sur QUOI, OÙ (éventuellement QUAND)
2. dans la description, coller le contenu de l'invitation
3. si des dates sont proposées, les reporter dans "Date de début"
4. enregistrer

NB : il est possible de mentionner un utilisateur Framaboard dans le contenu (ex: "pour toi @pyg ?") ou d'assigner l'invitation à une personne lors de la création. Il est possible de commenter la tâche.

Liaison avec Framateam
------------------------
Framateam permet à l'équipe en charge des invitations d'être tenu au courant des actions des autres membres de l'équipe, sans avoir à être connecté sur Framaboard.

1. se logguer sur framateam
2. créer un canal dédié (recommandé). Ex: "Invitations"
3. inviter les membres souhaités sur ce canal
4. dans le menu de l'équipe (en haut à droite, les 3 points verticaux), cliquer sur "Intégration"
5. choisir "webhooks entrants"
6. Ajouter un nouveau webhook entrant. Ex: "Evenements asso", lui donner une description, et indiquer le canal "Invitations" comme destination. Valider.
7. Dans le menu "notifications", cocher "Mattermost" et valider
8. Copier le lien du webhook
9. Aller sur Framaboard
10. Dans Préférences > Intégrations > Mattermost, copier le lien (laisser le champs "canal" vide), et valider

Logiquement, un déplacement d'une tâche d'une colonne à une autre, doit maintenant se répercuter de Framaboard à Framateam.


Ajouter des notifications mails
--------------------------------

Imaginons qu'on souhaite peaufiner :
* en attribuant une couleur automatique à la carte/post-it représentant une tâche, lorsqu'on la change de colonne ;
* en envoyant un mail à mail@machin.com (qui peut évidemment aussi être une liste) lorsqu'on bascule une invitation dans "événement programmé"
* en "fermant" les tâches qui seront placées dans la colonne "invitation refusées"
* en renvoyant une relance mail si une tâche est dans la colonne "invitations en attente" depuis 7j
* etc

Dans Framaboard
1. Préférences > Actions automatisées
2. Définir les actions automatisées voulues
3. TODO : A COMPLETER


Calendrier
================

Framaboard dispose de son propre calendrier, mais ce n'est pas très pratique de jongler entre différents calendriers.
L'objectif va donc être :
* d'afficher le calendrier des tâches dans le calendrier global de l'association (sur Framagenda)
* que chaque membre de l'équipe "invitations" puisse afficher cet agenda dans ses outils communs (ex: Thunderbird, calendrier smartphone, etc)

Afficher dans le calendrier global de l'asso
-------------------------------------------------
1. sur Framaboard, activer le calendrier public via "Menu > Préférences > Accès public"
2. faire un clic droit sur "évenement iCal" et copier ce lien
3. (optionnel) aller sur https://frama.link et faire un raccourci de ce lien (ex: frama.link/calinvit). Cela servira uniquement plus tard pour l'intégration smartphone
4. aller sur framagenda.org
5. dans la colonne de gauche, cliquer sur "ajouter un abonnement", et coller le lien de l'étape 2. 
6. Valider. Le calendrier devrait apparaître

Ajouter au smartphone
------------------------
(vous devez avoir installer l'application ICSDroid avant)
1. ouvrir un navigateur sur le téléphone
2. aller sur le lien framalink défini à l'étape 3 précédente, cela devrait vous proposer d'ajouter le lien via ICSDroid. Valider.
3. dans ICSDroid, confirmer l'ajout du calendrier
 


